<div class="schedule-construct-panel-title">
	<h2>ACADEMIC PROGRAMS</h2>
</div>
<div class="schedule-construct-faculties">
	<div class="schedule-construct-faculty-block">
		<h3 class="schedule-construct-faculty-name">Computer Science - IT</h3>
		<div class="schedule-construct-faculty-academic-programs-block">
			<div class="schedule-construct-faculty-academic-programs-wrap">
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 1</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 2</div>
    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 3</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
			</div>
			<div class="schedule-construct-faculty-academic-programs-wrap">
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 4</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 5</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 6</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
			</div>
			<div class="schedule-construct-faculty-academic-programs-wrap">
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 7</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 8</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
				<div class="schedule-construct-faculty-term-block">
					<div class="scheudle-faculty-term">Term 9</div>

    				<div class="schedule-construct-faculty-academic-programs-list">
    					<div class="schedule-construct-faculty-academic-program-cell">
    						<div class="schedule-construct-faculty-academic-program" title="Foreign Language (English 2)">Foreign Language (English 2)</div>
    						<div class="schedule-construct-faculty-academic-program-credits">5</div>
    					</div>
    					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Module of social-political education: Cultural studies">Module of social-political education: Cultural studies</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Mathematical Analisys 2">Mathematical Analisys 2</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">5</div>
       					</div>
       					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Algorithms and Data Structures">Algorithms and Data Structures</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	      					<div class="schedule-construct-faculty-academic-program" title="Introduction to Web Development">Introduction to Web Development</div>
	      					<div class="schedule-construct-faculty-academic-program-credits">5</div>
      					</div>
      					<div class="schedule-construct-faculty-academic-program-cell">
	       					<div class="schedule-construct-faculty-academic-program" title="Physical Culture">Physical Culture</div>
	       					<div class="schedule-construct-faculty-academic-program-credits">2</div>
	       				</div>
   					</div>
				</div>
			</div>
		</div>
	</div>
</div>