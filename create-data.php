<div class="form-create-schedule">
    <div class="form-group">
        <label>Course</label>
        <input type="text" name="course" class="form-control" value="<?php echo $course; ?>">
    </div>

    <div class="form-group search-field-group">
        <label>Group</label>
        <input type="text" name="edugroupid" class="form-control" value="<?php echo $edugroupid; ?>">
            <div class="schedule-search-block-group">
        <div class="search-result-group"></div>
        </div>
    </div>

    <div class="form-group search-field-teacher">
        <label>Teacher</label>
        <input type="text" name="teacherid" class="form-control search-field" value="<?php echo $teacherid; ?>">
            <div class="schedule-search-block-teacher">
        <div class="search-result-teacher"></div>
        </div>
    </div>

    <div class="form-group">
        <label>Lesson Type</label>
        <input type="text" name="lessontype" class="form-control" value="<?php echo $lessontype; ?>">
    </div>

    <div class="form-group">
        <label>Schedule Type</label>
        <input type="text" name="scheduletype" class="form-control" value="<?php echo $scheduletype; ?>">
    </div>

    <div class="form-group">
        <label>Time open</label>
        <input class="form-control" type="datetime-local" id="timeopen" name="timeopen"
               value="<?php echo $timeopen; ?>">
    </div>

    <div class="form-group">
        <label>Room</label>
        <input type="text" name="room" class="form-control" value="<?php echo $room; ?>">
    </div>
    <br><br>
    <input type="submit" class="" id="create-schedule-btn" value="Submit">
    <br><br>
</div>
<br><br>