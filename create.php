<?php
    require_once("config.php");

    $course = trim($_POST["course"]);
    $edugroupid = trim($_POST["edugroupid"]);
    $teacherid = trim($_POST["teacherid"]);
    $lessontype = trim($_POST["lessontype"]);
    $scheduletype = trim($_POST["scheduletype"]);
    $timeopen = trim($_POST["timeopen"]);
    $room = trim($_POST["room"]);

    $timeopen_db = str_replace("T"," ",$timeopen).":00";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $sql='INSERT INTO `schedule`(`course`, `edugroupid`, `teacherid`, `lessontype`, `scheduletype`, `timeopen`, `room`)
              VALUES ('.$course.', '.$edugroupid.', '.$teacherid.', "'.$lessontype.'", "'.$scheduletype.'", "'.$timeopen_db.'", "'.$room.'")';

        if ($mysqli->query($sql) === TRUE) {
            echo "Success";
            exit();
        } else {
            echo "Error: " . $sql . "<br>" . $mysqli->error;
        }

        $mysqli->close();
    }
?>