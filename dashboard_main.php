<div class="schedule-bottom-content">
					<img class="aitu_logo" src="lib/img/aitu-logo-3.png">
                    <div class="schedule-dashboard-select">
                        <div class="schedule-dashboard-select-block">
                            <div class="schedule-dashboard-select-section-wrap">
                                <div class="schedule-dashboard-select-section">
                                    <div class="schedule-dashboard-select-section-background"></div>
                                    <div class="schedule-dashboard-select-section-content">
                                        <div class="schedule-dashboard-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-dashboard-content-icon"></i>
                                            <p id="1">Schedule</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-dashboard-select-section">
                                    <div class="schedule-dashboard-select-section-background"></div>
                                    <div class="schedule-dashboard-select-section-content">
                                        <div class="schedule-dashboard-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-dashboard-content-icon"></i>
                                            <p id="2">Teachers</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-dashboard-select-section">
                                    <div class="schedule-dashboard-select-section-background"></div>
                                    <div class="schedule-dashboard-select-section-content">
                                        <div class="schedule-dashboard-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-dashboard-content-icon"></i>
                                            <p id="3">Subjects</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schedule-dashboard-select-section-wrap">
                                <div class="schedule-dashboard-select-section">
                                    <div class="schedule-dashboard-select-section-background"></div>
                                    <div class="schedule-dashboard-select-section-content">
                                        <div class="schedule-dashboard-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-dashboard-content-icon"></i>
                                            <p id="4">Rooms</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-dashboard-select-section">
                                    <div class="schedule-dashboard-select-section-background"></div>
                                    <div class="schedule-dashboard-select-section-content">
                                        <div class="schedule-dashboard-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-dashboard-content-icon"></i>
                                            <p id="5">Departments</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>