<?php
    require_once "config.php";

    $schedule = array();
    $rooms = array();
    $teachers = array();

    $requirements = array(
        "IT-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Module of social-political education: Cultural studies",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10
            ),
            "Subject-3" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            )
        ),
        "SE-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" =>"Discrete Mathematics",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-3" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            )
        ),
        "BD-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Modern History of Kazakhstan",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-3" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            )
        ),
        "IA-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Module of social-political education: Cultural studies",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10
            ),
            "Subject-3" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            )
        ),
        "MT-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-3" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            ),
            "Subject-6" => array(
                "discipline" => "Discrete Mathematics",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20,
            )
        ),
        "CS-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Module of social-political education: Cultural studies",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10
            ),
            "Subject-3" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            )
        ),
        "TS-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Algebra and Geometry",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-3" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            )
        ),
        "ITM-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Module of social-political education: Cultural studies",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10
            ),
            "Subject-3" => array(
                "discipline" => "Algebra and Geometry",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Management and Organization",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            ),
            "Subject-7" => array(
                "discipline" => "Business Informatics",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20,
            )
        ),
        "DJ-1" => array(
            "Subject-1" => array(
                "discipline" => "Foreign Language (English 2)",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-2" => array(
                "discipline" => "Module of social-political education: Cultural studies",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10
            ),
            "Subject-3" => array(
                "discipline" => "Mathematical Analisys 2",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-4" => array(
                "discipline" => "Algorithms and Data Structures",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-5" => array(
                "discipline" => "Introduction to Web Development",
                "active_hours" => 50,
                "lecture" => 20,
                "practice" => 20
            ),
            "Subject-6" => array(
                "discipline" => "Physical Culture",
                "active_hours" => 20,
                "lecture" => 10,
                "practice" => 10,
            )
        )
    );

    $academic_program = array(
        "IT",
        "SE",
        "BD",
        "IA",
        "MT",
        "CS",
        "TS",
        "ITM",
        "DJ"
    );

    $sql_schedule_group = "SELECT edugroup FROM edu_group";

    $result_schedule_group = $mysqli->query($sql_schedule_group);

    foreach ($result_schedule_group as $key) {
        $schedule[$key['edugroup']] = array();
    }

    $time_array = array("08:00", "08:50", "09:00", "09:50", "10:00", "10:50", "11:00", "11:50", "12:00", "12:50", "13:00", "13:50", "14:00", "14:50", "15:00", "15:50","16:00", "16:50", "17:00", "17:50");
    $week_array = array("2020-11-23","2020-11-24","2020-11-25","2020-11-26","2020-11-27");
    $weight_correlation = 0.000001;

    //get all rooms from DB

    $sql_room = "SELECT id,location,seats,type FROM rooms";

    $result_room = $mysqli->query($sql_room);

    //algorithm for generating schedule

    for ($i = 0; $i < 20; $i = $i + 4){ // time
        if($i == 8){
            continue;
        }

        //write down all inforamtion about rooms returned from DB, each time when pair of lessons passed

        $count = 0;
        foreach ($result_room as $room_code) {
            $rooms[$count] = array(
                "id" => $room_code['id'],
                "location" => $room_code['location'],
                "seats" => $room_code['seats'],
                "type" => $room_code['type']
            );
            $count++;
        }

        //look through each academic program

        foreach ($academic_program as $key) {
            $sql_group = "SELECT edu_group.id,edu_group.course,eduprogram,edugroup,numberofstudents FROM edu_group JOIN educational_program ON edu_group.eduprogramid = educational_program.id WHERE edugroup LIKE '".$key."-%' AND edu_group.course = 1";

            $result_group = $mysqli->query($sql_group);

            if ($result_group->num_rows == 0) {
                continue;
            }

            foreach ($result_group as $group) {
                $time_level = 0;
                $week_day_count = 0;
                $time_array_count = 0;

                for ($j = 0; $j < count($requirements[$key.'-1']); $j++) {
                    $discipline = $requirements[$key.'-1']['Subject-'.rand(1,count($requirements[$key.'-1']))];
                    $subtract = 0;
                    $lessontype = '';

                    if($discipline['active_hours'] == 50){
                        $subtract = 2;
                    } else if($discipline['active_hours'] == 20){
                        $subtract = 1;
                    }

                    $sql_teacher = "SELECT teacher.id,fullname,degree,hoursleft,weight,subjname FROM teacher JOIN subject ON teacher.subjid = subject.id WHERE subjname = '".$discipline['discipline']."' AND weight >= 1 AND hoursleft > 16 ORDER BY weight DESC LIMIT 1";

                    $result_teacher = $mysqli->query($sql_teacher);

                    for($j = 0; $j < 2; $j++){
                        if ($j == 0) {
                            $lessontype = "lecture";
                        } else {
                            $lessontype = "practice";
                        }

                        $flag = false;
                        $random_room;
                        if($lessontype == "lecture"){
                            while ($flag != true) {
                                $random_index = rand(0,count($rooms)-1);
                                if($rooms[$random_index]['type'] != "pc"){
                                    $random_room = $rooms[$random_index];
                                    unset($rooms[$random_index]);
                                    $flag = true;
                                }
                            }
                        } else {
                            while ($flag != true) {
                                $random_index = rand(0,count($rooms)-1);
                                if($rooms[$random_index]['type'] != "lecture"){
                                    $random_room = $rooms[$random_index];
                                    unset($rooms[$random_index]);
                                    $flag = true;
                                }
                            }
                        }

                        for($k = 0; $k < $subtract; $k++){

                            $teacher_id = '';

                            foreach ($result_teacher as $teacher) {
                                $teacher_id = $teacher['id'];

                                $schedule[$group['edugroup']][] = array(
                                    "course" => "1",
                                    "subjname" => $teacher['subjname'],
                                    "edugroupid" => $group['id'],
                                    "edugroup" => $group['edugroup'],
                                    "teacherid" => $teacher['id'],
                                    "fullname" => $teacher['fullname'],
                                    "degree" => $teacher['degree'],
                                    "lessontype" => $lessontype,
                                    "scheduletype" => "static",
                                    "timeopen" => $week_array[$week_day_count]." ".$time_array[$time_array_count].":00",
                                    "room" => $random_room['location']
                                );

                            }

                            $time_array_count = ($time_level * 2) + (($k+1) * 2);

                            if($time_array_count % 4 == 0 && $time_array_count != $time_level * 2){
                                $week_day_count += 1;
                                if($week_day_count == 5){
                                    $time_level += 2;
                                    $week_day_count = 0;
                                }
                                if($time_level == 0){
                                    $time_array_count = 0;
                                } else {
                                    $time_array_count = $time_level * 2;
                                }
                                if ($time_array_count == 20) {
                                    $time_array_count = 0;
                                    $week_day_count = 0;
                                    $time_level = 0;
                                }
                            }

                            $sql_current_teacher_update = "UPDATE teacher JOIN subject ON teacher.subjid = subject.id SET weight = weight - ".$weight_correlation.", hoursleft = hoursleft - ".$subtract." WHERE teacher.id = '".$teacher_id."';";

                            $mysqli->query($sql_current_teacher_update);

                            $sql_teacher_update = "UPDATE teacher JOIN subject ON teacher.subjid = subject.id SET weight = weight + ".$weight_correlation." WHERE teacher.id != ".$teacher_id." AND subjname = '".$discipline['discipline']."';";

                            $mysqli->query($sql_teacher_update);

                            $weight_correlation += 0.000001;
                        }
                    }
                }
            }
        }
    }

    foreach ($schedule as $group) {
        echo '<div class="schedule-table-search">
                <h1>'.$group[0]['edugroup'].'</h1>
              </div>
              <div class="schedule-bottom-content">
                <img class="aitu_logo" src="lib/img/aitu-logo-3.png">
                <div class="schedule-table schedule-table-display">
                <div class="schedule-table-row">
                    <div class="schedule-table-column"></div>
                    <div class="schedule-table-column"><p>MON</p><p>Monday</p></div>
                    <div class="schedule-table-column"><p>TUE</p><p>Tuesday</p></div>
                    <div class="schedule-table-column"><p>WED</p><p>Wednesday</p></div>
                    <div class="schedule-table-column"><p>THU</p><p>Thursday</p></div>
                    <div class="schedule-table-column"><p>FRI</p><p>Friday</p></div>
                    <div class="schedule-table-column"><p>SAT</p><p>Saturday</p></div>
                    <div class="schedule-table-column"><p>SUN</p><p>Sunday</p></div>
                </div>';
                        for($i = 0; $i < 20; $i = $i + 2){
                            echo '<div class="schedule-table-row">
                                    <div class="schedule-table-row-attr">
                                        <p>'.$time_array[$i].'</p>
                                        <p>'.$time_array[$i+1].'</p>
                                    </div>';
                            for($j = 1; $j < 8; $j++){
                                $count = 0;
                                foreach($group as $row){
                                    $count++;
                                    $timeopen = $row['timeopen'];
                                    $dt = new DateTime($timeopen);
                                    if($dt->format("N") == $j){
                                        if($time_array[$i] == $dt->format("H:i")){
                                            echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr">
                                                    <div class="schedule-table-row-attr-data">
                                                        <div class="schedule-table-row-attr-data-subject" title="'.$row['subjname'].'">
                                                            <p><i class="fa fa-pencil schedule-search-today-icon"></i>'.$row['subjname'].'</p>
                                                        </div>
                                                        <div class="schedule-table-row-attr-data-group" title="Group: '.$row['edugroup'].'">
                                                            <p><i class="fa fa-users schedule-search-today-icon"></i>Group: '.$row['edugroup'].'</p>
                                                        </div>
                                                        <div class="schedule-table-row-attr-data-teacher" title="Teacher: '.$row['fullname'].' ('.$row['position'].', '.$row['degree'].')">
                                                            <p><i class="fa fa-user schedule-search-today-icon"></i>Teacher: '.$row['fullname'].' ('.$row['position'].', '.$row['degree'].')</p>
                                                        </div>
                                                        <div class="schedule-table-row-attr-data-type-room">
                                                            <p><i class="fa fa-graduation-cap schedule-search-today-icon"></i>'.$row['room'].' '.$row['lessontype'].'</p>
                                                        </div>
                                                    </div>
                                                 </div>';
                                            $count--;
                                            break;
                                        }
                                    }
                                }
                                if($count == count($group)){
                                    echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr"></div>';
                                }
                            }
                            echo '</div>';
                        }
        echo '</div>
            </div>';
    }
    // Close connection
    $mysqli->close();
?>