<?php
    $get_condition = $_POST['condition'];
    date_default_timezone_set("Asia/Almaty");
                    // Include config file
                    require_once "config.php";
                    $today = new DateTime('now');
                    $start_week_day = new DateTime();
                    switch ($today->format("l")) {
                        case 'Monday':
                            $start_week_day->setTimestamp($today->getTimestamp());
                            break;
                        case 'Tuesday':
                            $today->modify("-1 days");
                            $start_week_day->setTimestamp($today->getTimestamp());
                            break;
                        case 'Wednesday':
                            $today->modify("-2 days");
                            $start_week_day->setTimestamp($today->getTimestamp());
                            break;
                        case 'Thursday':
                            $today->modify("-3 days");
                            $start_week_day->setTimestamp($today->getTimestamp());
                            break;
                        case 'Friday':
                            $today->modify("-4 days");
                            $start_week_day->setTimestamp($today->getTimestamp());
                            break;
                        case 'Saturday':
                            $today->modify("-5 days");
                            $start_week_day->setTimestamp($today->getTimestamp());
                            break;
                        case 'Sunday':
                            $today->modify("-6 days");
                            $start_week_day->setTimestamp($today->getTimestamp());
                            break;
                        default:
                            break;
                    }
                    $status = ' schedule-table-column-active';
               echo '<div class="schedule-table-search">
                        <div class="schedule-search-block">
                            <i class="fa fa-search schedule-search-block-icon"></i>
                            <input class="search-field" placeholder="Search Group, Teacher" type="text">
                            <div class="search-result"></div>
                        </div>
                        <div class="schedule-search-today">
                            <p><i class="fas fa-calendar-day schedule-search-today-icon"></i>Today</p>
                            <p>'.date("F").' '.date("Y").'</p>
                        </div>
                    </div>
                    <div class="schedule-bottom-content">
                        <img class="aitu_logo" src="lib/img/aitu-logo-3.png">
                        <div class="schedule-table schedule-table-display">
                            <div class="schedule-table-row">
                                <div class="schedule-table-column"></div>
                                <div class="schedule-table-column';
                                if(date("d") == $start_week_day->format("d")){
                                    echo $status;
                                }
                                echo '"><p>'.$start_week_day->format("d").'</p><p>'.$start_week_day->format("l").'</p>
                                </div>';
                                for($i = 0; $i  < 6; $i++){
                                    echo '<div class="schedule-table-column';
                                        $start_week_day->modify("+1 days");
                                        if(date("d") == $start_week_day->format("d")){
                                            echo $status;
                                        }
                                        echo '">';
                                        echo '<p>'.$start_week_day->format("d").'</p><p>'.$start_week_day->format("l").'</p>
                                        </div>';
                                }
                     echo '</div>';
                    // Attempt select query execution
                    $sql = "SELECT schedule.id,lessontype,scheduletype,timeopen,fullname,degree,position,subjname,edugroup,room FROM schedule INNER JOIN teacher on schedule.teacherid = teacher.id INNER JOIN subject on teacher.subjid = subject.id INNER JOIN edu_group ON schedule.edugroupid = edu_group.id ".$get_condition." GROUP BY schedule.id ORDER BY timeopen ASC";
                    $time_array = array("08:00", "08:50", "09:00", "09:50", "10:00", "10:50", "11:00", "11:50", "12:00", "12:50", "13:00", "13:50", "14:00", "14:50", "15:00", "15:50","16:00", "16:50", "17:00", "17:50");
                    if($result = $mysqli->query($sql)){
                        if($result->num_rows > 0){
                            for($i = 0; $i < 20; $i = $i + 2){
                                echo '<div class="schedule-table-row">
                                        <div class="schedule-table-row-attr';
                                        if(date("H:00") == $time_array[$i] && date("i") < 50){
                                            echo ' schedule-table-attr-active';
                                        }
                                        echo '">
                                            <p>'.$time_array[$i].'</p>
                                            <p>'.$time_array[$i+1].'</p>
                                        </div>';
                                for($j = 1; $j < 8; $j++){
                                    $count = 0;
                                    foreach($result as $row){
                                        $count++;
                                        $timeopen = $row['timeopen'];
                                        $dt = new DateTime($timeopen);
                                        if($dt->format("N") == $j){
                                            if($time_array[$i] == $dt->format("H:i")){
                                                echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr ';
                                                if(date("d") == $dt->format("d")){
                                                    if(date("H:00") == $dt->format("H:00") && date("i") < 50){
                                                        echo 'schedule-table-row-attr-block-active';
                                                    }
                                                }
                                                echo '">
                                                        <div class="schedule-table-row-attr-data">
                                                            <div class="schedule-table-row-attr-data-subject" title="'.$row['subjname'].'">
                                                                <p><i class="fas fa-pencil-alt schedule-search-today-icon"></i>'.$row['subjname'].'</p>
                                                            </div>
                                                            <div class="schedule-table-row-attr-data-group" title="Group: '.$row['edugroup'].'">
                                                                <p><i class="fa fa-users schedule-search-today-icon"></i>Group: '.$row['edugroup'].'</p>
                                                            </div>
                                                            <div class="schedule-table-row-attr-data-teacher" title="Teacher: '.$row['fullname'].' ('.$row['position'].', '.$row['degree'].')">
                                                                <p><i class="fa fa-user schedule-search-today-icon"></i>Teacher: '.$row['fullname'].' ('.$row['position'].', '.$row['degree'].')</p>
                                                            </div>
                                                            <div class="schedule-table-row-attr-data-type-room">
                                                                <p><i class="fa fa-graduation-cap schedule-search-today-icon"></i>Online '.$row['lessontype'].'</p>
                                                            </div>
                                                        </div>
                                                     </div>';
                                                $count--;
                                                break;
                                            }
                                        }
                                    }
                                    if($count == $result->num_rows){
                                        echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr"></div>';
                                    }
                                }
                                echo '</div>';
                            }
                        } else {
                            for($i = 0; $i < 20; $i = $i + 2){
                                echo '<div class="schedule-table-row">
                                        <div class="schedule-table-row-attr';
                                        if(date("H:00") == $time_array[$i] && date("i") < 50){
                                            echo ' schedule-table-attr-active';
                                        }
                                        echo '">
                                            <p>'.$time_array[$i].'</p>
                                            <p>'.$time_array[$i+1].'</p>
                                        </div>';
                                for($j = 1; $j < 8; $j++){
                                    echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr"></div>';
                                }
                                echo '</div>';
                            }
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
                    }
                    echo '</div>
                        </div>';
                    $result->free();
                    // Close connection
                    $mysqli->close();
?>