<?php
    require_once "config.php";

    $group = trim($_POST['group']);

    $time_array = array("08:00", "08:50", "09:00", "09:50", "10:00", "10:50", "11:00", "11:50", "12:00", "12:50", "13:00", "13:50", "14:00", "14:50", "15:00", "15:50","16:00", "16:50", "17:00", "17:50");

            $sql_group_lesson = "SELECT schedule.id,lessontype,scheduletype,timeopen,fullname,degree,position,subjname,edugroup,room FROM schedule INNER JOIN teacher on schedule.teacherid = teacher.id INNER JOIN subject on teacher.subjid = subject.id INNER JOIN edu_group ON schedule.edugroupid = edu_group.id WHERE edugroup LIKE '%".$group."%' GROUP BY schedule.id ORDER BY timeopen ASC";

          echo '<div class="schedule-table-container">
                  <div class="schedule-table-search">
                    <h1>'.$group.'</h1>
                  </div>
                  <div class="schedule-bottom-content">
                    <img class="aitu_logo" src="lib/img/aitu-logo-3.png">
                    <div class="schedule-table schedule-table-display">
                    <div class="schedule-table-row" style="border-left: 1px solid rgba(0,0,0,.3)">
                        <div class="schedule-table-column"></div>
                        <div class="schedule-table-column"><p>MON</p><p>Monday</p></div>
                        <div class="schedule-table-column"><p>TUE</p><p>Tuesday</p></div>
                        <div class="schedule-table-column"><p>WED</p><p>Wednesday</p></div>
                        <div class="schedule-table-column"><p>THU</p><p>Thursday</p></div>
                        <div class="schedule-table-column"><p>FRI</p><p>Friday</p></div>
                    </div>';
                    if($result_group_lesson = $mysqli->query($sql_group_lesson)){
                        if($result_group_lesson->num_rows > 0){
                            for($i = 0; $i < 20; $i = $i + 2){
                                echo '<div class="schedule-table-row" style="border-left: 1px solid rgba(0,0,0,.3)">
                                        <div class="schedule-table-row-attr">
                                            <p>'.$time_array[$i].'</p>
                                            <p>'.$time_array[$i+1].'</p>
                                        </div>';
                                for($j = 1; $j < 6; $j++){
                                    $count = 0;
                                    foreach($result_group_lesson as $row){
                                        $count++;
                                        $timeopen = $row['timeopen'];
                                        $dt = new DateTime($timeopen);
                                        if($dt->format("N") == $j){
                                            if($time_array[$i] == $dt->format("H:i")){
                                                echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr">
                                                        <div class="schedule-table-row-attr-data">
                                                            <div class="schedule-table-row-attr-data-subject" title="'.$row['subjname'].'">
                                                                <p><i class="fas fa-pencil-alt schedule-search-today-icon"></i>'.$row['subjname'].'</p>
                                                            </div>
                                                            <div class="schedule-table-row-attr-data-group" title="Group: '.$row['edugroup'].'">
                                                                <p><i class="fa fa-users schedule-search-today-icon"></i>Group: '.$row['edugroup'].'</p>
                                                            </div>
                                                            <div class="schedule-table-row-attr-data-teacher" title="Teacher: '.$row['fullname'].' ('.$row['position'].', '.$row['degree'].')">
                                                                <p><i class="fa fa-user schedule-search-today-icon"></i>Teacher: '.$row['fullname'].' ('.$row['position'].', '.$row['degree'].')</p>
                                                            </div>
                                                            <div class="schedule-table-row-attr-data-type-room">
                                                                <p><i class="fa fa-graduation-cap schedule-search-today-icon"></i>'.$row['room'].' '.$row['lessontype'].'</p>
                                                            </div>
                                                        </div>
                                                     </div>';
                                                $count--;
                                                break;
                                            }
                                        }
                                    }
                                    if($count == $result_group_lesson->num_rows){
                                        echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr"></div>';
                                    }
                                }
                                echo '</div>';
                            }
                        } else {
                            for($i = 0; $i < 20; $i = $i + 2){
                                echo '<div class="schedule-table-row">
                                        <div class="schedule-table-row-attr';
                                        if(date("H:00") == $time_array[$i] && date("i") < 50){
                                            echo ' schedule-table-attr-active';
                                        }
                                        echo '">
                                            <p>'.$time_array[$i].'</p>
                                            <p>'.$time_array[$i+1].'</p>
                                        </div>';
                                for($j = 1; $j < 6; $j++){
                                    echo '<div id="'.$i.'_'.$j.'" class="schedule-table-row-attr"></div>';
                                }
                                echo '</div>';
                            }
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
                    }
            echo ' </div>
                  </div>
                </div>';
    // Close connection
    $mysqli->close();
?>