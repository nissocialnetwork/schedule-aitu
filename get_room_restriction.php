<?php
	require_once("config.php");
	$time_array = array("08:00", "08:50", "09:00", "09:50", "10:00", "10:50", "11:00", "11:50", "12:00", "12:50", "13:00", "13:50", "14:00", "14:50", "15:00", "15:50","16:00", "16:50", "17:00", "17:50");
	$week_array = array("2020-11-23","2020-11-24","2020-11-25","2020-11-26","2020-11-27");
	$code = trim($_POST['code']);
	$group = trim($_POST['group']);
	$week_day = trim($_POST['week_day']) - 1;
	$time_range = trim($_POST['time_range']);
	$datetime = $week_array[$week_day].' '.$time_array[$time_range].':00';
	$sql = "SELECT timeopen,fullname,subjname,edugroup,room FROM schedule INNER JOIN rooms ON schedule.room = rooms.code INNER JOIN edu_group ON schedule.edugroupid = edu_group.id INNER JOIN teacher ON schedule.teacherid = teacher.id INNER JOIN subject ON teacher.subjid = subject.id WHERE timeopen = '".$datetime."' AND schedule.room LIKE '%".$code."%'";
	$result = $mysqli->query($sql);
	if($result->num_rows > 0){
		foreach ($result as $key) {
			$timeopen = $key['timeopen'];
            $dt = new DateTime($timeopen);
			echo '<div class="search-result-block"><p style="padding-left: 1em; padding-right: 1em;" class="group-restriction">'.$key['room'].' has already been taken by '.$key['fullname'].', group '.$key['edugroup'].' on '.$dt->format("l").' '.$dt->format("H:00").'-'.$dt->format("H:50").'</p></div>';
		}
	} else {
		echo "Free";
	}
?>