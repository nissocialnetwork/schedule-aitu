<?php
	echo '<div class="schedule-table-search">
					<div class="schedule-search-block">
						<i class="fa fa-search schedule-search-block-icon"></i>
						<input class="search-field" placeholder="Search Group, Teacher" type="text">
                        <div class="search-result"></div>
					</div>
					<div class="schedule-search-today">
						<p><i class="fas fa-calendar-day schedule-search-today-icon"></i>Today</p>
						<p>'.date("F").' '.date("Y").'</p>
					</div>
				</div>
				<div class="schedule-bottom-content">
					<img class="aitu_logo" src="lib/img/aitu-logo-3.png">
                    <div class="schedule-eduprogram-select">
                        <div class="schedule-eduprogram-select-block">
                            <div class="schedule-eduprogram-select-section-wrap">
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="1">Computer Science</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="2">Software Engineering</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="3">Big Data Analysis</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schedule-eduprogram-select-section-wrap">
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="4">Industrial Automation</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="5">Media Technologies</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="6">Cyber Security</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schedule-eduprogram-select-section-wrap">
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="7">Telecommunication Systems</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="8">IT Management</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="9">Digital Journalism</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
?>