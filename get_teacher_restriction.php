<?php
	require_once("config.php");
	$time_array = array("08:00", "08:50", "09:00", "09:50", "10:00", "10:50", "11:00", "11:50", "12:00", "12:50", "13:00", "13:50", "14:00", "14:50", "15:00", "15:50","16:00", "16:50", "17:00", "17:50");
	$week_array = array("2020-11-23","2020-11-24","2020-11-25","2020-11-26","2020-11-27");
	$teacherid = trim($_POST['id']);
	$subject = trim($_POST['subject']);
	$group = trim($_POST['group']);
	$week_day = trim($_POST['week_day']) - 1;
	$time_range = trim($_POST['time_range']);
	$datetime = $week_array[$week_day].' '.$time_array[$time_range].':00';
	$sql = "SELECT timeopen,fullname,hoursleft,subjname,edugroup,room FROM schedule INNER JOIN teacher on schedule.teacherid = teacher.id INNER JOIN subject on teacher.subjid = subject.id INNER JOIN edu_group ON schedule.edugroupid = edu_group.id WHERE timeopen = '".$datetime."' AND teacher.id = '".$teacherid."'";
	$result = $mysqli->query($sql);
	if($result->num_rows > 0){
		foreach ($result as $key) {
			$timeopen = $key['timeopen'];
            $dt = new DateTime($timeopen);
			echo '<div class="search-result-block"><p style="padding-left: 1em; padding-right: 1em;" class="group-restriction">'.$key['fullname'].' have another lesson for '.$key['edugroup'].' on '.$dt->format("l").' '.$dt->format("H:00").'-'.$dt->format("H:50").'</p></div>';
		}
	} else {
		$flag = false;
		foreach ($result as $key) {
			if($key['hoursleft'] == 0){
				$flag = true;
			}
		}
		if($flag == false){
			echo "Free";
		} else {
			echo '<div class="search-result-block"><p style="padding-left: 1em; padding-right: 1em;" class="group-restriction">This teacher has reached limit in week hours</p></div>';
		}
	}
?>