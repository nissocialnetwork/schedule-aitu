                    <?php
                    // Include config file
                    require_once "config.php";

                    // Attempt select query execution
                    $sql = "SELECT * FROM `schedule`";
                    if($result = $mysqli->query($sql)){
                        if($result->num_rows > 0){
                            echo '<div class="schedule-table-dashboard">';
                            echo '<a class="button-back" href="manager_interface.php" style="text-decoration: none;">Back</a><br>';
                            echo "<table id='schedule'>";
                                echo "<thead>";
                                    echo "<tr>";
                                        echo "<th>ID</th>";
                                        echo "<th>Course</th>";
                                        echo "<th>Edu group ID</th>";
                                        echo "<th>Teacher ID</th>";
                                        echo "<th>Lesson type</th>";
                                        echo "<th>Schedule type</th>";
                                        echo "<th>Time open</th>";
                                        echo "<th>Room</th>";
                                        echo "<th>Action</th>";
                                    echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                                while($row = $result->fetch_array()){
                                    echo "<tr>";
                                        echo "<td><b>" . $row['id'] . "</b></td>";
                                        echo "<td>" . $row['course'] . "</td>";
                                        echo "<td>" . $row['edugroupid'] . "</td>";
                                        echo "<td>" . $row['teacherid'] . "</td>";
                                        echo "<td>" . $row['lessontype'] . "</td>";
                                        echo "<td>" . $row['scheduletype'] . "</td>";
                                        echo "<td>" . $row['timeopen'] . "</td>";
                                        echo "<td>" . $row['room'] . "</td>";
                                        echo "<td>";
                                            echo '<a id="'. $row['id'] .'" class="schedule-edit-lesson-icon" title="Update Record" data-toggle="tooltip">
                                            <i class="fas fa-pencil-ruler"></i></a>';
                                            echo '<a id="'. $row['id'] .'" title="Delete Record" data-toggle = "tooltip">
                                            <i class="fas fa-trash-alt"></i></a>';
                                        echo "</td>";
                                    echo "</tr>";
                                }
                                echo "</tbody>";
                            echo "</table>";
                            echo '</div>';
                            // Free result set
                            $result->free();
                        } else{
                            echo "<p class='lead'><em>No records were found.</em></p>";
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
                    }

                    // Close connection
                    $mysqli->close();
                    ?>