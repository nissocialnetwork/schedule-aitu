<?php
	require_once "config.php";
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Schedule AITU</title>
		<link rel="stylesheet" href="lib/fontawesome/css/all.min.css">
		<link rel="stylesheet" href="lib/css/style.css">
        <link rel="stylesheet" href="lib/css/preloader.css">
		<link rel="shortcut icon" href="/lib/img/favicons.png" type="image/x-icon">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    	<script type="text/javascript" src="lib/js/script.js"></script>
	</head>
	<body>
        <div class="preloader">
            <div class="preloaderContent">
                <img class="preloader_logo" src="lib/img/aitu-logo-white.png">
                <div class="preloaderFirst"></div>
                <div class="preloaderSecond"></div>
                <div class="preloaderThird"></div>
            </div>
        </div>
		<div class="schedule-interface">
			<div class="schedule-top-block">
				<div class="schedule-lang">
					<div class="schedule-lang-block">
						<div class="lang">en</div>
						<div class="lang-flag">
							<img src="https://astanait.edu.kz/wp-content/plugins/polylang/flags/gb.png" width="20" height="13">
						</div>
					</div>
					<div class="schedule-lang-block">
						<div class="lang">ru</div>
						<div class="lang-flag">
							<img src="https://astanait.edu.kz/wp-content/plugins/polylang/flags/ru.png" width="20" height="13">
						</div>
					</div>
					<div class="schedule-lang-block">
						<div class="lang">kz</div>
						<div class="lang-flag">
							<img src="https://astanait.edu.kz/wp-content/plugins/polylang/flags/kz.png" width="20" height="13">
						</div>
					</div>
				</div>
				<div class="schedule-nav">
					<div class="schedule-nav-block schedule-nav-block-active">Schedule - Group</div>
					<div class="schedule-nav-block">Schedule - Teacher</div>
				</div>
			</div>
			<div class="schedule-bottom-block">
				<div class="schedule-table-search">
					<div class="schedule-search-block">
						<i class="fa fa-search schedule-search-block-icon"></i>
						<input class="search-field" placeholder="Search Group, Teacher" type="text">
                        <div class="search-result"></div>
					</div>
					<div class="schedule-search-today">
						<p><i class="fas fa-calendar-day schedule-search-today-icon"></i>Today</p>
						<p><?php echo date("F").' '.date("Y") ?></p>
					</div>
				</div>
				<div class="schedule-bottom-content">
					<img class="aitu_logo" src="lib/img/aitu-logo-3.png">
                    <div class="schedule-eduprogram-select">
                        <div class="schedule-eduprogram-select-block">
                            <div class="schedule-eduprogram-select-section-wrap">
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="1">Computer Science</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="2">Software Engineering</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="3">Big Data Analysis</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schedule-eduprogram-select-section-wrap">
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="4">Industrial Automation</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="5">Media Technologies</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="6">Cyber Security</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schedule-eduprogram-select-section-wrap">
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="7">Telecommunication Systems</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="8">IT Management</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="schedule-eduprogram-select-section">
                                    <div class="schedule-eduprogram-select-section-background"></div>
                                    <div class="schedule-eduprogram-select-section-content">
                                        <div class="schedule-eduprogram-select-section-content-data">
                                            <i class="fa fa-graduation-cap schedule-eduprogram-content-icon"></i>
                                            <p id="9">Digital Journalism</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</body>
</html>