<?php
	$time_array = array("08:00", "08:50", "09:00", "09:50", "10:00", "10:50", "11:00", "11:50", "12:00", "12:50", "13:00", "13:50", "14:00", "14:50", "15:00", "15:50","16:00", "16:50", "17:00", "17:50");
	$week_array = array("2020-11-23","2020-11-24","2020-11-25","2020-11-26","2020-11-27");
	$group = trim($_POST['group']);
	$week_day = trim($_POST['week_day']) - 1;
	$time_range = trim($_POST['time_range']);
?>
<div class="schedule-construct-insert-lesson-title">
	<h1><?php echo $group ?></h1>
	<h2><?php
		switch ($week_day) {
            case '0':
                echo 'Monday ';
                break;
            case '1':
                echo 'Tuesday ';
                break;
            case '2':
                echo 'Wednesday ';
                break;
            case '3':
                echo 'Thursday ';
                break;
            case '4':
                echo 'Friday ';
                break;
            default:
                break;
        }
        echo $time_array[$time_range].'-'.$time_array[$time_range+1];
	?></h2>
</div>
<div class="schedule-construct-insert-lesson-data-block">
	<span>Subject</span>
	<div class="schedule-construct-insert-lesson-input-box">
        <i class="fa fa-search schedule-construct-insert-lesson-search-block-icon"></i>
        <input class="search-subject-field" placeholder="Subject" type="text">
        <div class="search-subject-result"></div>
	</div>
</div>
<div class="schedule-construct-insert-lesson-data-block">
	<span>Teacher</span>
	<div class="schedule-construct-insert-lesson-input-box">
        <i class="fa fa-search schedule-construct-insert-lesson-search-block-icon"></i>
        <input class="search-teacher-field" placeholder="Teacher" type="text">
        <div class="search-teacher-result"></div>
	</div>
</div>
<div class="schedule-construct-insert-lesson-data-block">
	<span>Room</span>
	<div class="schedule-construct-insert-lesson-input-box">
        <i class="fa fa-search schedule-construct-insert-lesson-search-block-icon"></i>
        <input class="search-room-field" placeholder="Room" type="text">
        <div class="search-room-result"></div>
	</div>
</div>
<div class="schedule-construct-insert-lesson-data-block">
	<div class="schedule-construct-insert-lesson-select-box">
		<div class="schedule-construct-insert-lesson-select-box-option">
			<p>Lecture</p>
			<i class="fas fa-book"></i>
		</div>
		<div class="schedule-construct-insert-lesson-select-box-option">
			<p>Practice</p>
			<i class="far fa-keyboard"></i>
		</div>
	</div>
</div>
<div class="schedule-construct-insert-lesson-btn schedule-construct-insert-lesson-btn-inactive">Publish</div>