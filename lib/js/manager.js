$(document).ready(function(){
	var group, week_day, time_range;
	var box1 = false, box2 = false, box3 = false, box4 = false;
	function check_conditions(){
		if(box1 == true && box2 == true && box3 == true && box4 == true){
			$('.schedule-construct-insert-lesson-btn').removeClass('schedule-construct-insert-lesson-btn-inactive');
		} else {
			if($('.schedule-construct-insert-lesson-btn').hasClass('schedule-construct-insert-lesson-btn-inactive') == false){
				$('.schedule-construct-insert-lesson-btn').addClass('schedule-construct-insert-lesson-btn-inactive');
			}
		}
	}
	$('body').on('click','.schedule-construct-insert-lesson-btn',function(){
		if($(this).hasClass('schedule-construct-insert-lesson-btn-inactive') == false){
			var subject = $('.search-subject-field').val(),
			teacher = $('.search-teacher-field').val().split(":id-")[1],
			teacher_name = $('.search-teacher-field').val().split(":id-")[0],
			room = $('.search-room-field').val(),
			lessontype = '';
			$('.schedule-construct-insert-lesson-select-box-option').each(function(){
				if($(this).hasClass('schedule-construct-insert-lesson-select-box-option-selected') == true){
					lessontype = $(this).find('p').html();
				}
			});
			$.ajax({
				url: 'insert_lesson_data.php',
				method: 'POST',
				data: {
					group: group,
					week_day: week_day,
					time_range: time_range,
					subject: subject,
					lessontype: lessontype,
					teacher: teacher,
					room: room
				},
				success: function(data){
					if(data == "SuccessISuccessU"){
						$('.schedule-construct-insert-lesson-block').html('<i class="far fa-check-circle search-insert-lesson-success"></i>');
						var group_block;
						$('.schedule-table-container').each(function(){
							if($(this).find('.schedule-table-search').find('h1').html().localeCompare(group) == 0){
								group_block = $(this);
							}
						});
						group_block.find('.schedule-table-row-attr').each(function(){
							if($(this).attr('id') == time_range+'_'+week_day){
								$(this).html('<div class="schedule-table-row-attr-data">'+
                                                '<div class="schedule-table-row-attr-data-subject" title="'+subject+'">'+
                                                    '<p><i class="fas fa-pencil-alt schedule-search-today-icon"></i>'+subject+'</p>'+
                                                '</div>'+
                                                '<div class="schedule-table-row-attr-data-group" title="Group: '+group+'">'+
                                                    '<p><i class="fa fa-users schedule-search-today-icon"></i>Group: '+group+'</p>'+
                                                '</div>'+
                                                '<div class="schedule-table-row-attr-data-teacher" title="Teacher: '+teacher_name+'">'+
                                                    '<p><i class="fa fa-user schedule-search-today-icon"></i>Teacher: '+teacher_name+'</p>'+
                                                '</div>'+
                                                '<div class="schedule-table-row-attr-data-type-room">'+
                                                    '<p><i class="fa fa-graduation-cap schedule-search-today-icon"></i>'+room+' '+lessontype+'</p>'+
                                                '</div>'+
                                            '</div>');
							}
						});
						setTimeout(function(){
							$('.schedule-construct-insert-lesson').fadeOut(100);
							$('.schedule-construct-insert-lesson').css('left','-100%');
						},1500);
					} else {
						$('.schedule-construct-insert-lesson-block').append(data);
					}
				}
			});
		}
	});
	$.ajax({
		url: 'dashboard_main.php',
		success: function(data){
			$(".schedule-bottom-block").html(data);
		},
	});
	$('.schedule-construct-group-academic-programs-btn').on('click',function(){
		$.ajax({
			url: 'academic_programs.php',
			success: function(data){
				$(".schedule-construct-left-panel-block").html(data);
			}
		});
		if($(this).hasClass('active')){
			$('.schedule-construct-left-panel').fadeOut(100);
			$('.schedule-construct-left-panel').css('left','-100%');
		} else {
			$('.schedule-construct-left-panel').fadeIn(100);
			$('.schedule-construct-left-panel').css('left','0');
		}
		$(this).toggleClass('active');
	});
	$('.schedule-construct-waybill-btn').on('click',function(){
		$.ajax({
			url: 'waybill.php',
			success: function(data){
				$(".schedule-construct-left-panel-block").html(data);
			}
		});
		if($(this).hasClass('active')){
			$('.schedule-construct-left-panel').fadeOut(100);
			$('.schedule-construct-left-panel').css('left','-100%');
		} else {
			$('.schedule-construct-left-panel').fadeIn(100);
			$('.schedule-construct-left-panel').css('left','0');
		}
		$(this).toggleClass('active');
	});
	$('body').on('click','.schedule-table-container',function(){
		var flag = false;
		$('.schedule-nav-block').each(function(){
			if($(this).hasClass('schedule-nav-block-active') && $(this).html().localeCompare("Construct - Schedule") == 0){
				flag = true;
			}
		});
		if (flag == true) {
			$('.schedule-construct-group-name').find('h2').html($(this).find('.schedule-table-search').find('h1').html());
		}
	});
	$('body').on('click','.schedule-table-row-attr',function(){
		if($(this).html() == '') {
			group = $(this).parent().parent().parent().parent().find('h1').html();
			week_day = $(this).attr('id').split('_')[1];
			time_range = $(this).attr('id').split('_')[0];
			$('.schedule-construct-insert-lesson').fadeIn(100);
			if(group == "IT-2001"){
				$('.schedule-construct-group-requirements').html('<h3>Requirements left</h3>'+
        			'<div class="schedule-construct-group-requirements-list">'+
        				'<div class="schedule-construct-group-requirement-block">'+
        					'<p>Foreign Language (English 2) - <span class="group-restriction">20 hours</span></p>'+
        				'</div>'+
        				'<div class="schedule-construct-group-requirement-block">'+
        					'<p>Module of social-political education: Cultural studies - <span class="group-restriction">20 hours</span></p>'+
        				'</div>'+
        				'<div class="schedule-construct-group-requirement-block">'+
        					'<p>Mathematical Analisys 2 - <span class="group-restriction">10 hours</span></p>'+
        				'</div>'+
        				'<div class="schedule-construct-group-requirement-block">'+
        					'<p>Algorithms and Data Structures - <span class="group-restriction">30 hours</span></p>'+
        				'</div>'+
        				'<div class="schedule-construct-group-requirement-block">'+
        					'<p>Introduction to Web Development - <span class="group-restriction">20 hours</span></p>'+
        				'</div>'+
        				'<div class="schedule-construct-group-requirement-block">'+
        					'<p>Physical Culture - <span class="group-restriction">20 hours</span></p>'+
        				'</div>'+
        			'</div>'+
        			'<div class="schedule-construct-group-academic-programs-btn-block">'+
        				'<div class="schedule-construct-waybill-btn" title="Waybill, quick search of groups">'+
        					'<i class="fa fa-list"></i>'+
        					'<p>Waybill, quick search of groups</p>'+
        				'</div>'+
        			'</div>');
			}
			$.ajax({
				url: 'insert_lesson.php',
				method: 'POST',
				data: {
					group: group,
					week_day: week_day,
					time_range: time_range
				},
				success: function(data){
					$(".schedule-construct-insert-lesson-block").html(data);
					$('.schedule-construct-insert-lesson').css('left','0');
				},
			});
		}
	});
	$("body").on("click",".search-result-block",function(){
		if($(this).find("span").attr("id") == "subjects"){
			var subject = $(this).find("p").html();
			$(this).parent().parent().find('input').val(subject);
			$(this).parent().parent().prepend('<i style="right: 1em;" class="far fa-check-circle search-result-block-icon"></i>');
			box1 = true;
			$(".search-subject-result").html('');
			check_conditions();
		} else if($(this).find("span").attr("id") == "teachers"){
			var id = $(this).find("p").attr("id");
			var subject = $('.search-subject-field').val();
			$(this).parent().parent().find('input').val($(this).find("p").html()+":id-"+$(this).find("p").attr('id'));
			var block = $(this);
			$.ajax({
				url: 'get_teacher_restriction.php',
				method: 'POST',
				data: {
					id: id,
					subject: subject,
					group: group,
					week_day: week_day,
					time_range: time_range
				},
				success: function(data){
					if(data == "Free"){
						block.parent().parent().find('.search-result-block-icon-wrong').fadeOut(100);
						block.parent().parent().prepend('<i style="right: 1em;" class="far fa-check-circle search-result-block-icon"></i>');
						box2 = true;
						$(".search-teacher-result").html('');
					} else {
						block.parent().parent().find('.search-result-block-icon').fadeOut(100);
						block.parent().parent().prepend('<i style="right: 1em;" class="far fa-times-circle search-result-block-icon-wrong"></i>');
						box2 = false;
						$(".search-teacher-result").html(data);
					}
					check_conditions();
				},
			});
		} else if($(this).find("span").attr("id") == "rooms"){
			var code = $(this).find("p").html();
			$(this).parent().parent().find('input').val($(this).find("p").html());
			var block = $(this);
			$.ajax({
				url: 'get_room_restriction.php',
				method: 'POST',
				data: {
					code: code,
					group: group,
					week_day: week_day,
					time_range: time_range
				},
				success: function(data){
					if(data == "Free"){
						block.parent().parent().find('.search-result-block-icon-wrong').fadeOut(100);
						block.parent().parent().prepend('<i style="right: 1em;" class="far fa-check-circle search-result-block-icon"></i>');
						box3 = true;
						$(".search-room-result").html('');
					} else {
						block.parent().parent().find('.search-result-block-icon').fadeOut(100);
						block.parent().parent().prepend('<i style="right: 1em;" class="far fa-times-circle search-result-block-icon-wrong"></i>');
						box3 = false;
						$(".search-room-result").html(data);
					}
					check_conditions();
				},
			});
		}
	});
	$('body').on('click','.schedule-construct-insert-lesson-background',function(){
		$('.schedule-construct-insert-lesson').fadeOut(100);
		$('.schedule-construct-insert-lesson').css('left','-100%');
	});
	$('body').on('focusin','.schedule-construct-insert-lesson-data-block',function(){
		$(this).css('border','1px solid #557daa');
		$(this).css('background','#557daa');
		$(this).find('span').css('color','#fff');
		$(this).find('.schedule-construct-insert-lesson-search-block-icon').css('color','#557daa');
		$(this).find('input').css('border','1px solid #557daa');
		$(this).find('input').css('border-bottom','none');
		$(this).find('input').css('border-bottom-left-radius','none');
		$(this).find('input').css('border-bottom-right-radius','none');
		if($(this).find('span').html().localeCompare("Subject") == 0){
			var value = $('.search-subject-field').val();
			$.ajax({
				url: 'search-subject.php',
				method: 'POST',
				data: {
					str: value
				},
				success: function(data){
					$(".search-subject-result").html(data);
				},
			});
		} else if($(this).find('span').html().localeCompare("Teacher") == 0){
			var value = $('.search-teacher-field').val();
			var subject = $('.search-subject-field').val();
			if(subject == ''){
				$(".search-teacher-result").html('<div class="search-result-block"><p class="group-restriction" style="padding-left: 1em;">Subject was not selected</p></div>');
			} else {
				$.ajax({
					url: 'search-teacher.php',
					method: 'POST',
					data: {
						str: value,
						subject: subject
					},
					success: function(data){
						if(data != ''){
							$(".search-teacher-result").html(data);
						} else {
							$(".search-teacher-result").html('<div class="search-result-block"><p style="padding: 0.2em 1em;">No matching was found or wrong subject name</p></div>');
						}
					},
				});
			}
		} else if($(this).find('span').html().localeCompare("Room") == 0){
			var value = $('.search-room-field').val();
			$.ajax({
				url: 'search-room.php',
				method: 'POST',
				data: {
					str: value
				},
				success: function(data){
					$(".search-room-result").html(data);
				},
			});
		}
	});
	$('body').on('focusout','.schedule-construct-insert-lesson-data-block',function(){
		$(this).css('border','1px solid rgba(0,0,0,.3)');
		$(this).css('background','transparent');
		$(this).find('span').css('color','rgba(0,0,0,.8)');
		$(this).find('.schedule-construct-insert-lesson-search-block-icon').css('color','rgba(0,0,0,.6)');
		$(this).find('input').css('border','1px solid rgba(0,0,0,.3)');
	});
	$('body').on('click','.schedule-construct-insert-lesson-select-box-option',function(){
		var flag = false;
		box4 = false;
		if($(this).hasClass('schedule-construct-insert-lesson-select-box-option-selected') == true){
			flag = true;
		}
		$('.schedule-construct-insert-lesson-select-box-option').removeClass('schedule-construct-insert-lesson-select-box-option-selected');
		if(flag == false){
			box4 = true;
			$(this).addClass('schedule-construct-insert-lesson-select-box-option-selected');
		}
		check_conditions();
	});
	$('body').on('click','.schedule-edit-lesson-icon',function(){
		$('.schedule-construct-update-lesson').fadeIn(100);
		$('.schedule-construct-update-lesson').css('left','0');
	});
	$('body').on("keyup",".search-waybill-field",function(){
		var value = $(this).val();
		if($(this).val().length > 1){
			$.ajax({
				url: 'search-group.php',
				method: 'POST',
				data: {
					str: value
				},
				success: function(data){
					$(".search-waybill-result").html(data);
				},
			});
		} else {
			$(".search-waybill-result").html('');
		}
	});
	$("body").on("click",".search-result-waybill-block",function(){
		if($(this).find("span").attr("id") == "groups"){
			var group = $(this).find("p").html();
			$.ajax({
				url: 'get_group_data_waybill.php',
				method: 'POST',
				data: {
					group: group
				},
				success: function(data){
					$(".schedule-construct-left-panel-block").find('.schedule-table-container').remove();
					$(".schedule-construct-left-panel-block").append(data);
					$(".search-waybill-result").html('');
				},
			});
		}
	});
	$('body').on("keyup",".search-subject-field",function(){
		var value = $(this).val();
		if($(this).val().length > 0){
			$.ajax({
				url: 'search-subject.php',
				method: 'POST',
				data: {
					str: value
				},
				success: function(data){
					$(".search-subject-result").html(data);
				},
			});
		} else {
			$(".search-subject-result").html('');
		}
	});
	$('body').on("keyup",".search-teacher-field",function(){
		var value = $(this).val(),
		subject = $('.search-subject-field').val();
		if($(this).val().length > 0){
			if(subject == ''){
				$(".search-teacher-result").html('<div class="search-result-block"><p class="group-restriction" style="padding-left: 1em;">Subject was not selected</p></div>');
			} else {
				$.ajax({
					url: 'search-teacher.php',
					method: 'POST',
					data: {
						str: value,
						subject: subject
					},
					success: function(data){
						if(data != ''){
							$(".search-teacher-result").html(data);
						} else {
							$(".search-teacher-result").html('<div class="search-result-block"><p style="padding: 0.2em 1em;">No matching was found or wrong subject name</p></div>');
						}
					},
				});
			}
		} else {
			$(".search-teacher-result").html('');
		}
	});
	$('body').on("keyup",".search-room-field",function(){
		var value = $(this).val();
		if($(this).val().length > 0){
			$.ajax({
				url: 'search-room.php',
				method: 'POST',
				data: {
					str: value
				},
				success: function(data){
					$(".search-room-result").html(data);
				},
			});
		} else {
			$(".search-room-result").html('');
		}
	});
	$('body').on('click','#create-schedule-btn',function(){
		var course, edugroupid, teacherid, lessontype, scheduletype, timeopen, room;
		course = $("input[name='course']").val();
		edugroupid = $("input[name='edugroupid']").val();
		teacherid = $("input[name='teacherid']").val();
		lessontype = $("input[name='lessontype']").val();
		scheduletype = $("input[name='scheduletype']").val();
		timeopen = $("input[name='timeopen']").val();
		room = $("input[name='room']").val();
		$.ajax({
			url: 'create.php',
			method: 'POST',
			data: {
				course: course,
				edugroupid: edugroupid,
				teacherid: teacherid,
				lessontype: lessontype,
				scheduletype: scheduletype,
				timeopen: timeopen,
				room: room
			},
			success: function(data){
				if(data == "Success"){
					$("body").prepend('<div class="preloader">'+
							'<div class="preloaderContent">'+
				                '<img class="preloader_logo" src="lib/img/aitu-logo-white.png">'+
				                '<div class="preloaderFirst"></div>'+
				                '<div class="preloaderSecond"></div>'+
				                '<div class="preloaderThird"></div>'+
            				'</div>'
       					 +'</div>');
					$.ajax({
						url: 'create-data.php',
						success: function(data){
							$(".schedule-bottom-block").html(data);
						},
					});
				}
			},
		});
	});
});