$(document).ready(function(){
	function preloader(time) {
		$(() => {

			setInterval(() => {
				let p = $('.preloader');

				p.css('opacity', 0);

				setInterval(
					() => p.remove(),
					parseInt(p.css('--duration')) * 1000
				)
			}, time);
		});
	}
	preloader(700);
	function get_preloader(time){
		$("body").prepend('<div class="preloader">'+
							'<div class="preloaderContent">'+
				                '<img class="preloader_logo" src="lib/img/aitu-logo-white.png">'+
				                '<div class="preloaderFirst"></div>'+
				                '<div class="preloaderSecond"></div>'+
				                '<div class="preloaderThird"></div>'+
            				'</div>'
       					 +'</div>');
		preloader(time);
	}
	var eduprogram,group = "1",course = "1";
	$('body').on("keyup",".search-field",function(){
		var value = $(this).val();
		if($(this).val().length > 1){
			$.ajax({
				url: 'search.php',
				method: 'POST',
				data: {
					str: value
				},
				success: function(data){
					$(".search-result").html(data);
				},
			});
		} else {
			$(".search-result").html('');
		}
	});
	$("body").on("click",".search-result-block",function(){
		if($(this).find("span").attr("id") == "groups"){
			var condition = "WHERE schedule.edugroupid = '"+$(this).find("p").attr("id")+"'";
			$.ajax({
				url: 'get_group_data.php',
				method: 'POST',
				data: {
					condition: condition
				},
				success: function(data){
					$(".schedule-bottom-block").html(data);
				},
			});
		} else if($(this).find("span").attr("id") == "teacher"){
			var condition = "WHERE schedule.teacherid = '"+$(this).find("p").attr("id")+"'";
			$.ajax({
				url: 'get_teacher_data.php',
				method: 'POST',
				data: {
					condition: condition
				},
				success: function(data){
					$(".schedule-bottom-block").html(data);
				},
			});
		}
	});
	$("body").on("click",".schedule-search-today",function(){
		var dt = new Date();
		var time = (dt.getHours() - 8)*2, week_day = dt.getDay();
		$("#"+time+"_"+week_day).addClass("schedule-table-row-attr-pulse");
		$('html, body').animate({
            scrollTop: $("#"+time+"_"+week_day).offset().top - 200
        }, 500);
        setTimeout(function(){
        	$("#"+time+"_"+week_day).delay(500).removeClass("schedule-table-row-attr-pulse");
        },5500);
	});
	$("body").on("focusout",".schedule-search-block",function(){
		$(this).find("input").css("border","1px solid rgba(0,0,0,.3)");
		$(this).find(".schedule-search-block-icon").css("color","rgba(0,0,0,.3)");
	});
	$("body").on("focusin",".schedule-search-block",function(){
		$(this).find("input").css("border","1px solid #557daa");
		$(this).find(".schedule-search-block-icon").css("color","#557daa");
	});
	$("body").on("click",".schedule-eduprogram-select-section",function(){
		eduprogram = $(this).find(".schedule-eduprogram-select-section-content-data").find("p").attr("id");
		var condition = "WHERE edu_group.eduprogramid = '"+eduprogram+"'"+" AND schedule.edugroupid = '"+group+"'"+" AND schedule.course = '"+course+"'";
    	$.ajax({
			url: 'get_group_data.php',
			method: 'POST',
			data: {
				condition: condition
			},
			success: function(data){
				$(".schedule-bottom-block").html(data);
				get_preloader(200);
			},
		});
	});
	$("body").on("click",".schedule-dashboard-select-section",function(){
		dashboard = $(this).find(".schedule-dashboard-select-section-content-data").find("p").attr("id");
        if(dashboard == 1){
           $.ajax({
			url: 'index-1.php',
			method: 'POST',
			data: {
				condition: dashboard
			},
			success: function(data){
				$(".schedule-bottom-block").html(data);
				get_preloader(200);
			},
		});
           }
        if(dashboard == 2){
           $.ajax({
			url: 'index-2.php',
			method: 'POST',
			data: {
				condition: dashboard
			},
			success: function(data){
				$(".schedule-bottom-block").html(data);
				get_preloader(200);
			},
		});
           }
        if(dashboard == 3){
           $.ajax({
			url: 'index-3.php',
			method: 'POST',
			data: {
				condition: dashboard
			},
			success: function(data){
				$(".schedule-bottom-block").html(data);
				get_preloader(200);
			},
		});
           }
        if(dashboard == 4){
           $.ajax({
			url: 'index-4.php',
			method: 'POST',
			data: {
				condition: dashboard
			},
			success: function(data){
				$(".schedule-bottom-block").html(data);
				get_preloader(200);
			},
		});
           }
        if(dashboard == 5){
           $.ajax({
			url: 'index-5.php',
			method: 'POST',
			data: {
				condition: dashboard
			},
			success: function(data){
				$(".schedule-bottom-block").html(data);
				get_preloader(200);
			},
		});
           }

	});
	$(".schedule-nav-block").on('click',function(){
		if($(this).hasClass("schedule-nav-block-active") == false){
			$(".schedule-nav-block").removeClass("schedule-nav-block-active");
			$(this).addClass("schedule-nav-block-active");
			$(".schedule-bottom-content").html('');
			if($(this).html().localeCompare("Schedule - Group") == 0){
				$(".schedule-bottom-block").html('');
				$('.schedule-construct-right-panel').css('display','none');
				$.ajax({
					url: 'get_schedule.php',
					success: function(data){
						$(".schedule-bottom-block").html(data);
					},
				});
			} else if($(this).html().localeCompare("Schedule - Teacher") == 0){
				$(".schedule-bottom-block").html('');
				$('.schedule-construct-right-panel').css('display','none');
				$.ajax({
					url: 'get_teacher_data.php',
					success: function(data){
						$(".schedule-bottom-block").html(data);
					},
				});
			} else if($(this).html().localeCompare("Construct - Schedule") == 0){
				$(".schedule-bottom-block").html('');
				$("body").prepend('<div class="preloader">'+
							'<div class="preloaderContent">'+
				                '<img class="preloader_logo" src="lib/img/aitu-logo-white.png">'+
				                '<div class="preloaderFirst"></div>'+
				                '<div class="preloaderSecond"></div>'+
				                '<div class="preloaderThird"></div>'+
            				'</div>'
       					 +'</div>');
				$.ajax({
					url: 'construct_schedule.php',
					success: function(data){
						$(".schedule-bottom-block").html(data);
						$('.schedule-construct-right-panel').css('display','block');
					},
				});
			} else if($(this).html().localeCompare("Create - Schedule") == 0){
				$(".schedule-bottom-block").html('');
				$('.schedule-construct-right-panel').css('display','none');
				$("body").prepend('<div class="preloader">'+
							'<div class="preloaderContent">'+
				                '<img class="preloader_logo" src="lib/img/aitu-logo-white.png">'+
				                '<div class="preloaderFirst"></div>'+
				                '<div class="preloaderSecond"></div>'+
				                '<div class="preloaderThird"></div>'+
            				'</div>'
       					 +'</div>');
				$.ajax({
					url: 'create-data.php',
					success: function(data){
						$(".schedule-bottom-block").html(data);
					},
				});
			} else if($(this).html().localeCompare("Dashboard") == 0){
				$(".schedule-bottom-block").html('');
				$('.schedule-construct-right-panel').css('display','none');
				$("body").prepend('<div class="preloader">'+
							'<div class="preloaderContent">'+
				                '<img class="preloader_logo" src="lib/img/aitu-logo-white.png">'+
				                '<div class="preloaderFirst"></div>'+
				                '<div class="preloaderSecond"></div>'+
				                '<div class="preloaderThird"></div>'+
            				'</div>'
       					 +'</div>');
				$.ajax({
					url: 'dashboard_main.php',
					success: function(data){
						$(".schedule-bottom-block").html(data);
					},
				});
			}
		}
	});
});