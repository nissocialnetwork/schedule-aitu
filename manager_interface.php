<?php
	require_once "config.php";
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Schedule AITU</title>
		<link rel="stylesheet" href="lib/fontawesome/css/all.min.css">
		<link rel="stylesheet" href="lib/css/style.css">
        <link rel="stylesheet" href="lib/css/preloader.css">
        <link rel="stylesheet" href="lib/css/manager.css">
		<link rel="shortcut icon" href="/lib/img/favicons.png" type="image/x-icon">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    	<script type="text/javascript" src="lib/js/script.js"></script>
    	<script type="text/javascript" src="lib/js/manager.js"></script>
	</head>
	<body>
        <div class="preloader">
            <div class="preloaderContent">
                <img class="preloader_logo" src="lib/img/aitu-logo-white.png">
                <div class="preloaderFirst"></div>
                <div class="preloaderSecond"></div>
                <div class="preloaderThird"></div>
            </div>
        </div>
        <div class="schedule-construct-right-panel">
        	<div class="schedule-construct-right-panel-block">
        		<div class="schedule-construct-group-name">
        			<h2>GROUP</h2>
        		</div>
        		<div class="schedule-construct-group-academic-programs">
        			<h3>Academic programs for this trimestr</h3>
        			<div class="schedule-construct-group-academic-programs-list">
        				<div class="group-academic-program">
        					<p>Foreign Language (English 2) - <span class="group-academic-program-hour">40 hours</span></p>
        				</div>
        				<div class="group-academic-program">
        					<p>Module of social-political education: Cultural studies - <span class="group-academic-program-hour">20 hours</span></p>
        				</div>
        				<div class="group-academic-program">
        					<p>Mathematical Analisys 2 - <span class="group-academic-program-hour">40 hours</span></p>
        				</div>
        				<div class="group-academic-program">
        					<p>Algorithms and Data Structures - <span class="group-academic-program-hour">40 hours</span></p>
        				</div>
        				<div class="group-academic-program">
        					<p>Introduction to Web Development - <span class="group-academic-program-hour">40 hours</span></p>
        				</div>
        				<div class="group-academic-program">
        					<p>Physical Culture - <span class="group-academic-program-hour">20 hours</span></p>
        				</div>
        			</div>
        			<div class="schedule-construct-group-academic-programs-btn-block">
        				<div class="schedule-construct-group-academic-programs-btn" title="See the list of academic programs for all faculties">
        					<i class="fa fa-list"></i>
        					<p>See the list of academic programs for all faculties</p>
        				</div>
        			</div>
        		</div>
        		<div class="schedule-construct-group-requirements">
        			<h3>Requirements left</h3>
        			<div class="schedule-construct-group-requirements-list">
        				<div class="schedule-construct-group-requirement-block">
        					<p>Foreign Language (English 2) - <span class="group-restriction">40 hours</span></p>
        				</div>
        				<div class="schedule-construct-group-requirement-block">
        					<p>Module of social-political education: Cultural studies - <span class="group-restriction">20 hours</span></p>
        				</div>
        				<div class="schedule-construct-group-requirement-block">
        					<p>Mathematical Analisys 2 - <span class="group-restriction">40 hours</span></p>
        				</div>
        				<div class="schedule-construct-group-requirement-block">
        					<p>Algorithms and Data Structures - <span class="group-restriction">40 hours</span></p>
        				</div>
        				<div class="schedule-construct-group-requirement-block">
        					<p>Introduction to Web Development - <span class="group-restriction">40 hours</span></p>
        				</div>
        				<div class="schedule-construct-group-requirement-block">
        					<p>Physical Culture - <span class="group-restriction">20 hours</span></p>
        				</div>
        			</div>
        			<div class="schedule-construct-group-academic-programs-btn-block">
        				<div class="schedule-construct-waybill-btn" title="Waybill, quick search of groups">
        					<i class="fa fa-list"></i>
        					<p>Waybill, quick search of groups</p>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="schedule-construct-left-panel">
        	<div class="schedule-construct-left-panel-block">

        	</div>
        </div>
        <div class="schedule-construct-insert-lesson">
        	<div class="schedule-construct-insert-lesson-background"></div>
        	<div class="schedule-construct-insert-lesson-outer-block">
	        	<div class="schedule-construct-insert-lesson-block"></div>
        	</div>
        </div>
		<div class="schedule-interface">
			<div class="schedule-top-block">
				<div class="schedule-lang">
					<div class="schedule-lang-block">
						<div class="lang">en</div>
						<div class="lang-flag">
							<img src="https://astanait.edu.kz/wp-content/plugins/polylang/flags/gb.png" width="20" height="13">
						</div>
					</div>
					<div class="schedule-lang-block">
						<div class="lang">ru</div>
						<div class="lang-flag">
							<img src="https://astanait.edu.kz/wp-content/plugins/polylang/flags/ru.png" width="20" height="13">
						</div>
					</div>
					<div class="schedule-lang-block">
						<div class="lang">kz</div>
						<div class="lang-flag">
							<img src="https://astanait.edu.kz/wp-content/plugins/polylang/flags/kz.png" width="20" height="13">
						</div>
					</div>
				</div>
				<div class="schedule-nav">
					<div class="schedule-nav-block schedule-nav-block-active">Dashboard</div>
					<div class="schedule-nav-block">Construct - Schedule</div>
					<div class="schedule-nav-block">Create - Schedule</div>
				</div>
			</div>
			<div class="schedule-bottom-block">
				<div class="schedule-table-search">
					<div class="schedule-search-block">
						<i class="fa fa-search schedule-search-block-icon"></i>
						<input class="search-field" placeholder="Search Group, Teacher" type="text">
                        <div class="search-result"></div>
					</div>
				</div>
				<div class="schedule-bottom-content">
					<img class="aitu_logo" src="lib/img/aitu-logo-3.png">
					<div class="schedule-dashboard">
						<div class="schedule-dashboard-table">
							<div class="schedule-dashboard-table-block"></div>
						</div>
					</div>
                </div>
			</div>
		</div>
	</body>
</html>