<?php
	require_once "config.php";
	$str = $_POST['str'];

	$search_block = '';
	$query_room = "SELECT * FROM `rooms` WHERE `location` LIKE '%".$str."%'";
	$result_room = $mysqli->query($query_room);

	$count = 0;
	foreach ($result_room as $row) {
		if($row['type'] == 'pc'){
			$search_block .= '<div class="search-result-block">
                            <span id="rooms"><i class="fas fa-chalkboard-teacher schedule-search-today-icon"></i></span>
                            <p id="'.$row['location'].'">'.$row['location'].'</p>
                          </div>';
		} else {
			$search_block .= '<div class="search-result-block">
                            <span id="rooms"><i class="fas fa-desktop schedule-search-today-icon"></i></span>
                            <p id="'.$row['location'].'">'.$row['location'].'</p>
                          </div>';
		}
        $count++;
        if($count == 6){
        	break;
        }
	}

	echo $search_block;
?>