<?php
	require_once "config.php";
	$str = $_POST['str'];

	$search_block = '';
	$query_teacher = "SELECT * FROM `teacher` WHERE `fullname` LIKE '%".$str."%' ORDER BY fullname";
	$query_group = "SELECT * FROM `edu_group` WHERE `edugroup` LIKE '%".$str."%'";
	$result_group = $mysqli->query($query_group);
	$result_teacher = $mysqli->query($query_teacher);

    $sql_room = "SELECT id,location,seats,type FROM rooms";

    $result_room = $mysqli->query($sql_room);

	$count = 0;
	foreach ($result_group as $row) {
		$search_block .= '<div class="search-result-block">
                            <span id="groups"><i class="fa fa-users schedule-search-today-icon"></i></span>
                            <p id="'.$row['id'].'">'.$row['edugroup'].'</p>
                          </div>';
        $count++;
        if($count == 6 && $result_teacher->num_rows >=6 ){
        	$count = 0;
        	break;
        }
        else if($count == 7 && $result_teacher->num_rows >= 5){
        	$count = 0;
        	break;
        }
        else if($count == 8 && $result_teacher->num_rows >= 4){
        	$count = 0;
        	break;
        }
        else if($count == 9 && $result_teacher->num_rows >= 3){
        	$count = 0;
        	break;
        }
        else if($count == 10 && $result_teacher->num_rows >= 2){
        	$count = 0;
        	break;
        }
        else if($count == 11 && $result_teacher->num_rows >= 1){
        	$count = 0;
        	break;
        }
        else if($count == 12 && $result_teacher->num_rows >= 0){
        	$count = 0;
        	break;
        }
	}

	foreach ($result_teacher as $row) {
		$search_block .= '<div class="search-result-block">
                            <span id="teacher"><i class="fa fa-user schedule-search-today-icon"></i></span>
                            <p id="'.$row['id'].'">'.$row['fullname'].'</p>
                          </div>';
        $count++;
        if($count == 6 && $result_group->num_rows >=6){
        	$count = 0;
        	break;
        }
        else if($count == 7 && $result_group->num_rows >= 5){
        	$count = 0;
        	break;
        }
        else if($count == 8 && $result_group->num_rows >= 4){
        	$count = 0;
        	break;
        }
        else if($count == 9 && $result_group->num_rows >= 3){
        	$count = 0;
        	break;
        }
        else if($count == 10 && $result_group->num_rows >= 2){
        	$count = 0;
        	break;
        }
        else if($count == 11 && $result_group->num_rows >= 1){
        	$count = 0;
        	break;
        }
        else if($count == 12 && $result_group->num_rows >= 0){
        	$count = 0;
        	break;
        }
	}

	echo $search_block;
?>