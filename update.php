<?php

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "aitu_schedule";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$id = trim($_POST["id"]);
$course = trim($_POST["course"]);
$eduprogramid = trim($_POST["eduprogramid"]);
$edugroupid = trim($_POST["edugroupid"]);
$teacherid = trim($_POST["teacherid"]);
$lessontype = trim($_POST["lessontype"]);
$scheduletype = trim($_POST["scheduletype"]);
$timeopen = trim($_POST["timeopen"]);
$room = trim($_POST["room"]);

$timeopen_db = str_replace("T"," ",$timeopen).":00";
 
// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    // Check input errors before inserting in database
        // Prepare an update statement
    
$sql = 'UPDATE `schedule` SET `id`='.$id.',`course`='.$course.',`edugroupid`='.$edugroupid.',`teacherid`='.$teacherid.',`lessontype`="'.$lessontype.'",`scheduletype`="'.$scheduletype.'",`timeopen`="'.$timeopen.'",`room`="'.$room.'" 
WHERE id = '.$id.'';
    
    if ($conn->query($sql) === TRUE) {
header("location: index.php");
exit();
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();       
}

else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
    // Include config file
    require_once "config.php";
    
    // Prepare a select statement
    $sql = "SELECT * FROM schedule WHERE id = ?";
    
    if($stmt = $mysqli->prepare($sql)){
        // Bind variables to the prepared statement as parameters
        $stmt->bind_param("i", $param_id);
        
        // Set parameters
        $param_id = trim($_GET["id"]);
        
        // Attempt to execute the prepared statement
        if($stmt->execute()){
            $result = $stmt->get_result();
            
            if($result->num_rows == 1){
                /* Fetch result row as an associative array. Since the result set
                contains only one row, we don't need to use while loop */
                $row = $result->fetch_array(MYSQLI_ASSOC);
                
                // Retrieve individual field value  
             $id = $row["id"];
             $course = $row["course"];
             $eduprogramid = $row["eduprogramid"];
             $edugroupid = $row["edugroupid"];
             $teacherid = $row["teacherid"];
             $lessontype = $row["lessontype"];
             $scheduletype = $row["scheduletype"];
             $timeopen = $row["timeopen"];
             $room = $row["room"];

            } else{
                // URL doesn't contain valid id parameter. Redirect to error page
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
     
    // Close statement
    $stmt->close();
    
    // Close connection
    $mysqli->close();
} else{
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
} 
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Update Schedule</h2>
                    </div>
                    <p>Please edit the input values and submit to update the record.</p>
                     <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                         
                         <div class="form-group <?php echo (!empty($id_err)) ? 'has-error' : ''; ?> hidden">
                            <label>ID</label>
                            <input type="text" name="id" class="form-control" value="<?php echo $id; ?>">
                            <span class="help-block"><?php echo $id_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($quiz_id_err)) ? 'has-error' : ''; ?>">
                            <label>course</label>
                            <input type="text" name="course" class="form-control" value="<?php echo $course; ?>">
                            <span class="help-block"><?php echo $course_err;?></span>
                        </div>
                    
                        
                        <div class="form-group <?php echo (!empty($group_name_err)) ? 'has-error' : ''; ?>">
                            <label>edugroupid</label>
                            <input type="text" name="edugroupid" class="form-control" value="<?php echo $edugroupid; ?>">
                            <span class="help-block"><?php echo $edugroupid_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($teacherid_err)) ? 'has-error' : ''; ?>">
                            <label>teacherid</label>
                            <input type="text" name="teacherid" class="form-control" value="<?php echo $teacherid; ?>">
                            <span class="help-block"><?php echo $teacherid_err;?></span>
                        </div>
                        
                        
                        <div class="form-group <?php echo (!empty($lessontype_err)) ? 'has-error' : ''; ?>">
                            <label>lessontype</label>
                            <input type="text" name="lessontype" class="form-control" value="<?php echo $lessontype; ?>">
                            <span class="help-block"><?php echo $lessontype_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($scheduletype_err)) ? 'has-error' : ''; ?>">
                            <label>scheduletype</label>
                            <input type="text" name="scheduletype" class="form-control" value="<?php echo $scheduletype; ?>">
                            <span class="help-block"><?php echo $scheduletype_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($timeopen_err)) ? 'has-error' : ''; ?>">
                            <label>timeopen</label>
                            <input class="form-control" type="datetime-local" id="timeopen" name="timeopen"
                                   value="<?php echo str_replace(" ","T",$timeopen); ?>">
                            <span class="help-block"><?php echo $timeopen_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($room_err)) ? 'has-error' : ''; ?>">
                            <label>room</label>
                            <input type="text" name="room" class="form-control" value="<?php echo $room; ?>">
                            <span class="help-block"><?php echo $room_err;?></span>
                        </div>
                        
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>